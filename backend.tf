terraform {
  backend "s3" {
    bucket = "mitch-gitlab-cicd"
    region = "eu-west-1"
    key    = "Gitlab/terraform.tfstate"
  }
}
#!/bin/bash

exec > >(tee -i /var/log/user-data.lo)
exec 2>&1
sudo apt update -y
sudo apt install software-properties-common -y
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible -y
sudo apt install git -y
mkdir Ansible && cd Ansible
pwd
git clone https://gitlab.com/devopscicd6855452/gitlab-cicd.git

cd gitlab-cicd/ANSIBLE
ansible-playbook -i localhost jenkins-playbook.yml
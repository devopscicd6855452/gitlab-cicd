# Secrity Groups
resource "aws_security_group" "jenkins-sg" {
  name        = "Jenkins-Security Group"
  description = "Jenkins-Server security group"

  ingress = [
    for port in [22, 80, 443, 8080] : {
      description      = "TLS from VPC"
      from_port        = port
      to_port          = port
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Jenkins-sg"
  }

}

# EC2 Instance

resource "aws_instance" "web" {
  ami                    = "ami-0905a3c97561e0b69"
  instance_type          = "t2.medium"
  key_name               = "terraform-key"
  vpc_security_group_ids = [aws_security_group.jenkins-sg.id]
  user_data              = templatefile("./install.sh", {})

  tags = {
    Name = "Jenkins-sonar"
  }
  root_block_device {
    volume_size = 15
  }
}